import React from "react";

const AddTaskForm = (props) => {
  return (
    <div className="task__form-wrap">
      <div className="row">
        <div className="col-md-12">
          <form className="task__form" onSubmit={props.submit}>
            <div className="input-group">
              <input type="text" onChange={props.change} className="form-control" placeholder="Add new task" />
              <span className="input-group-btn">
                <button className="btn btn-default" type="button">Add Task</button>
              </span>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AddTaskForm;