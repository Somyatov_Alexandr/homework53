import React, { Component } from 'react';
import './App.css';
import AddTaskForm from './AddTaskForm/AddTaskForm';
import Task from './Task/Task';

class App extends Component {
  state = {
    tasks: [
      {text: 'Задача номер 1', id: 1},
      {text: 'Задача номер 2', id: 2}
    ]
  }

  handleChangeValue = (event) => {
    // const task = {...this.state.tasks};
    let taskItem = {};
    const val = event.target.value;

    taskItem.text = val;
    taskItem.id = this.state.tasks.length + 1;
    
    // console.log(task[0]);
    return taskItem;
    
  }

  handleSubmit = (event) => {
    event.preventDefault();
    let taskItem = this.handleChangeValue(event);
    console.log(taskItem);
    this.setState({
      tasks: [
        ...this.state.tasks,
        taskItem
      ]
    });
  }

  render() {
    let tasks = null;
    
    if (this.state.tasks.length > 0) {
      tasks = (
        <div className="task__list">
          {
            this.state.tasks.map((task) => {
              return (
                <Task key={task.id} text={task.text} />
              );
            })
          }
        </div>
      );
    };
    return (
      <div className="App">
        <div className="task">
          <div className="container">
            <div className="row">
              <div className="col-md-8 col-md-offset-2">
                <h2>Список задач</h2>
                <AddTaskForm
                  submit={(event) => this.handleSubmit(event)}
                  change={(event) => this.handleChangeValue(event)}
                  />
                {tasks}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
