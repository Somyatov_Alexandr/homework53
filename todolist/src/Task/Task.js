import React from "react";
import './Task.css';

const Task = (props) => {
  return (
    <div className="task__item">
      <div className="row">
        <div className="col-md-11">
          {props.text}
        </div>
        <div className="col-md-1 text-center">
          <a className="remove__item">
            <i className="glyphicon glyphicon-trash"></i>
          </a>
        </div>
      </div>
    </div>
  );
}

export default Task;